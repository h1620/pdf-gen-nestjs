import { QRCodeCanvas } from '@loskir/styled-qr-code-node';
import { image } from 'pdfkit';
import axios from 'axios';
import { svgToPng } from './convertToPng';
// import  QRCodeCanvas  from "qr-code-styling-node";

export const generateQrCode = async (
  name: string,
  destinationLink: string,
  centerImageUrl: any,
  destDir: string,
) => {
  // fetch image from url with axios
  const response = await axios.get(centerImageUrl, {
    responseType: 'arraybuffer',
  });
  // convert image to base64
  const imageBase64 = (await svgToPng(response.data)).toString('base64')
  const qrCode = new QRCodeCanvas({
    width: 300,
    height: 300,
    data: destinationLink,
    image: 'data:image/png;base64,' + imageBase64,
    dotsOptions: {
      color: '#4267b2',
      type: 'rounded',
    },
    backgroundOptions: {
      color: '#e9ebee',
    },
    // imageOptions: {
    //     crossOrigin: "anonymous",
    //     margin: 20
    // }
  });
  return await qrCode.toBuffer('png');
};
