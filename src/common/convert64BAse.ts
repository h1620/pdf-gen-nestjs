import * as fs from 'fs';
import { resolve } from 'path';
export async function readFilePromise(path: string): Promise<Buffer> {
    return await new Promise((resolve, reject) => {
        return fs.readFile(path, (err, data) => {
            if (err) {
                reject(err);
            } else {
                resolve(data);
            }
        });
    })
}

export const convert64Base = async (path: string) => {
    const streamFile = await readFilePromise(path);
    return streamFile.toString('base64');
}