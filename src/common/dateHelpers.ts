export function getCurrentDateLongFormat() {
    const date = new Date();

    return date.toLocaleString('de-DE', {
        weekday: "long",
        year: "numeric",
        month: "long",
        day: "numeric",
    });
}
