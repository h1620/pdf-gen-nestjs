import * as sharp from 'sharp'
import { STATICSPATH } from './constants';

export async function svgToPng(buffer) {
    return await sharp(buffer)
        .png()
        .toBuffer();
}