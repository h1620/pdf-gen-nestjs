import { Body, Controller, Get, Param, Post, Query, Res } from '@nestjs/common';
import { AppService } from './app.service';
import { PdfService } from './pdf/pdf.service';
import { IncoiceQueryDto } from './dtos/invoice.dto';
import { Response } from 'express';
import { DBService } from './db/db.service';
import { channel } from 'diagnostics_channel';
@Controller('pdf-gen')
export class AppController {
  constructor(
    private readonly appService: AppService,
    private readonly pdfService: PdfService,
    private readonly dbService: DBService) { }

  @Get()
  getHello(): string {
    return this.appService.getHello();
  }
  @Get('invoce')
  getInvoice(
    @Query() { company, months }: IncoiceQueryDto,
    @Res() res: Response
  ) {
    if (!months) return res.send({ message: 'you need to specify the months', data: null })
    const pdfDocument = this.pdfService.createPdf();
    this.pdfService.addHeader(pdfDocument);
    this.pdfService.addText(pdfDocument, months);
    const columnNames = [{ property: 'id', label: 'ID', width: 100 }, { property: 'name', label: 'ID', width: 100 }, { property: 'email', label: 'ID', width: 100 }];
    const ids = months.split(',').map((item) => parseInt(item));

    const data = this.dbService.findData('user', ids);
    const table = this.pdfService.createTable(pdfDocument, columnNames, data);
    this.pdfService.writePdf(pdfDocument, res, 'invoice.pdf',);
  }

  @Post('channel-qr-code/:_uuid')
  async sendTemplate(@Res() res: Response, @Body() { companyNameTitle, companyName, companyLogo }: { companyNameTitle: string, companyName: string, companyLogo: string }, @Param() { _uuid }: { _uuid: string }) {
    const channel = this.dbService.findOne('channel', _uuid)
    if (!channel) return res.send({ message: 'there is no company found with this id', data: null })
    
    return await this.pdfService.generateTeamplate( res,companyNameTitle,companyName,companyLogo,channel);

  }
  
  @Post('channel-qr-code')
  async sendTemplateNoRouteParam(@Res() res: Response, @Body() { channelUuid: _uuid, companyNameTitle, companyName, companyLogo }: { channelUuid: string, companyNameTitle: string, companyName: string, companyLogo: string }) {
    return await this.pdfService.generateTeamplate( res,companyNameTitle,companyName,companyLogo, { _uuid });
  }
}
