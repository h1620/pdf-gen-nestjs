import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { PdfService } from './pdf/pdf.service';
import { DBService } from './db/db.service';

@Module({
  imports: [],
  controllers: [AppController],
  providers: [AppService, PdfService,DBService],
})
export class AppModule {}
