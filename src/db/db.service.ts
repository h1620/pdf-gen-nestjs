import { Injectable } from '@nestjs/common';
import * as data from './dataDummy.json';
import * as channelDataBase from './channels.json'
@Injectable()
export class DBService {
    private selectTable(tableName: string) {
        const tables = { channel: channelDataBase, user: data }
        return tables[`${tableName}`]

    }
    findData(TABLE: string, ids: number[]) {
        this.selectTable(TABLE)
        const result = data.filter((item) => {
            return ids.includes(item.id);
        })
        return result;
    }
    findOne(TABLE: string, id) {
        const table = this.selectTable(TABLE)
        const foundItem = table.find((item: any) => item[`${TABLE}`]._uuid == id)
        if (!foundItem) return null
        return foundItem[`${TABLE}`]

    }

}
