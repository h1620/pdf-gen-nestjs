import { Injectable } from '@nestjs/common';
import pdfkit from 'pdfkit-table';
import * as fs from 'fs';

import { join } from 'path';
import { KONFIDAL_LOGO_PNG_B64, KONFIDAL_LOGO_SVG_URL, KONFIDAL_LOGO_WITH_NAME, PDFHEADER, STATICSPATH, paragrath1, paragrath2, paragrath4, paragrath6, paragrath7 } from 'src/common/constants';
import { generateQrCode } from 'src/common/qrCode';
import { convert64Base } from 'src/common/convert64BAse';
import { getCurrentDateLongFormat } from 'src/common/dateHelpers';
import * as sharp from 'sharp';

@Injectable()
export class PdfService {
  validateMonths(): string {
    return 'Hello World!';
  }
  async writePdf(document, res, filename = '') {
    if (filename) document.pipe(fs.createWriteStream(join(STATICSPATH, filename)));
    document.pipe(res)
    document.end();
  }
  createPdf() {
    const pdfDocument = new pdfkit({
      size: 'A4',
      layout: 'portrait',
      margins: {
        top: 10,
        bottom: 50,
        left: 50,
        right: 50
      },
    })
    return pdfDocument;
  }

  addText(pdfDocument: pdfkit, text: string, fontsize: number = 12, options: any = {}) {
    if (fontsize) pdfDocument.fontSize(fontsize)
    const defaultSettingForText = {
      ellipsis: false,
      x: pdfDocument.x,
      y: pdfDocument.y,
      ...options,
      // lineBreak: false, // this make the next line start at the end of line before in x axy  and also does not allow you to center text
    };
    if (options.x || options.y) {
      pdfDocument
        .text(text, defaultSettingForText.x, defaultSettingForText.y, defaultSettingForText);

    } else {
      pdfDocument
        .text(text, defaultSettingForText);
    }
    pdfDocument.moveDown();
  }

  addHeader(pdfDocument: PDFKit.PDFDocument) {
    const today = new Date();

    const defaultSettingForText = {
      align: 'left',
      indent: 0,
      paragraphGap: 0,
      wordSpacing: 0,
      characterSpacing: 0,
      lineGap: 1,
      ellipsis: false,
    };

    pdfDocument.moveTo(10, 70)
      .lineTo(830, 70)
      .stroke();
    pdfDocument
      .moveDown(1)
      .fontSize(30)
      .text(PDFHEADER, defaultSettingForText)
      .lineGap(0)
      .fontSize(15)
      .text(today.toLocaleDateString(), 50, 50)
    pdfDocument.moveDown();
  }

  createTable(pdfDocument, columnNames: {}[], data: any[]) {
    pdfDocument.table({
      headers: columnNames,
      datas: data,
    });
    return pdfDocument;
  }

  async createReport(channel, res) {
    const pdfDocument = this.createPdf();
    this.addHeader(pdfDocument)
    this.addText(pdfDocument, channel.config.title, 26, { align: 'center', y: 150 })
    this.addText(pdfDocument, 'Scan the QR-code to access the reporting form', 20, { align: 'center', y: 180 });
    this.addText(pdfDocument, 'Further instructions are given on the web-portal.', 11, { align: 'center', y: 210 });

    const qrImageBuffer = await generateQrCode(channel._uuid, 'https://www.google.com', KONFIDAL_LOGO_SVG_URL, STATICSPATH);
    const qrWidth = 300;
    const imgx = (pdfDocument.page.width - qrWidth) / 2;

    pdfDocument.image(qrImageBuffer, imgx, 250, { width: qrWidth, height: 300 });

    this.addText(pdfDocument, 'Link to the reporting form', 11, { align: 'center', y: pdfDocument.y + 330 })
    this.addText(pdfDocument, `http://app.konfidal.eu/dev/portal/${channel._uuid}`, 11, { align: 'center', y: pdfDocument.y })

    if (channel.company.parent.name) {
      this.addText(pdfDocument, `This channel is operated by ${channel.company.parent.name} for`, 11, { align: 'center' })
      this.addText(pdfDocument, `${channel.company.name}`, 11, { align: 'center', y: pdfDocument.y + 20 })
    }
    await this.writePdf(pdfDocument, res,`${channel._uuid}.pdf` );
  }
  async generateTeamplate(res, companyNameTitle, companyName, companyLogo, channel) {
    const pdfDocument = this.createPdf();
    const titleYAxy = 50;
    const redirectURL = `http://app.konfidal.eu/dev/portal/${channel._uuid}`

    const originalCompanyLogo = Buffer.from(companyLogo, 'base64');
    const konfidalLogoBuffer = Buffer.from(KONFIDAL_LOGO_WITH_NAME, 'base64')
    const logoSize = await sharp(originalCompanyLogo).resize(800, 250).toBuffer();
    const logoSize2 = await sharp(konfidalLogoBuffer).resize(700, 160).toBuffer();

    pdfDocument.image(logoSize, 50, titleYAxy, { width: 150, height: 40 });
    pdfDocument.image(logoSize2, 350, titleYAxy, { width: 150, height: 40 });

    this.addText(pdfDocument, `Hinweisgeberportal bei ${companyNameTitle}`, 20, { y: 140 });
    this.addText(pdfDocument, paragrath1, 11);
    this.addText(pdfDocument, paragrath2(companyName), 11);
    this.addText(pdfDocument, 'Dieses Portal wird von der konfidal GmbH, Hauptstr. 28, 15806 Zossen im Auftrag betriebenund dient zur Abgabe von Hinweisen, die Missstände im Unternehmen betreffen. Hiermit wollen wir einen weiteren Schritt in Richtung Transparenz und Offenheit gehen und unsere Unternehmenskultur stärken.', 11)
    this.addText(pdfDocument, paragrath4, 11);
    this.addText(pdfDocument, `Unter diesem Link oder dem Scan des QR-Codes finden Sie das Portal:
${redirectURL}  
    `, 11);
    this.addText(pdfDocument, paragrath6, 11);
    this.addText(pdfDocument, paragrath7, 11)

    const todayDate = getCurrentDateLongFormat()

    this.addText(pdfDocument, todayDate, 11)
    this.addText(pdfDocument, `Die Geschäftsleitung`, 11)

    const qrImageBuffer = await generateQrCode(channel._uuid, redirectURL, KONFIDAL_LOGO_SVG_URL, STATICSPATH);
    const qrWidth = 200;
    const imgx = (pdfDocument.page.width - 250);

    pdfDocument.image(qrImageBuffer, imgx, 470, { width: qrWidth, height: 200 });
    await this.writePdf(pdfDocument, res);
  }
}
