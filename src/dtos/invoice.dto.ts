import { IsOptional, IsNumber, IsString } from 'class-validator';

export class IncoiceQueryDto {

  @IsOptional()
  @IsNumber()
  limit?: number;

  
  @IsString()
  company: string;

  @IsString()
  months?: string;
}
