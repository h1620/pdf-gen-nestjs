FROM node:18-alpine as install
RUN apk update && apk add fontconfig
# RUN mkdir -p /home/node/app/node_modules && chown -R node:node /home/node/app


WORKDIR /home/node/app

COPY package*.json ./
# USER node

RUN npm install 

COPY .  ./

FROM install as build

RUN npm run build

# FROM node:18-alpine
# RUN apk add --no-cache fontconfig

# COPY --from=build /home/node/app/dist ./dist
# COPY --from=build /home/node/app/package*.json ./
# COPY --from=build /home/node/app/node_modules ./node_modules
# COPY --from=build /home/node/app/statics ./statics

# ENV NODE_ENV production

EXPOSE 3000

CMD [ "npm", "run", "start:prod" ]
